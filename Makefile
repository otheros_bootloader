
CC=cc
LD=ld
OBJCOPY=objcopy
CFLAGS=-Wall -O2

SRC_otheros_bootloader_loader=otheros_bootloader_loader.S
OBJ_otheros_bootloader_loader=$(SRC_otheros_bootloader_loader:.S=.o)
TARGET_otheros_bootloader_loader=otheros_bootloader_loader.bin

PETITBOOT_IMAGE=dtbImage.ps3.bin
VFLASH_IMAGE=vflash.bin
VFLASH_DEV_DEFAULT=/dev/ps3vflashf
VFLASH_DEV_LINUX=/dev/ps3vflashh1
RAM_DEV=/dev/ps3ram
SLL_LOAD_LV2_OFFSET=0x00165e44
LOG2_PAGE_SIZE_OFFSET=0x000a7dd0 + 0x0000001b0
VFLASH_REGION_OFFSET=0x006b3f80 + 0x00000018

$(TARGET_otheros_bootloader_loader): $(OBJ_otheros_bootloader_loader)
	$(OBJCOPY) -O binary $^ $@

SRC_mkimage=mkimage.c
OBJ_mkimage=$(SRC_mkimage:.c=.o)
TARGET_mkimage=mkimage

$(TARGET_mkimage): $(OBJ_mkimage)
	$(CC) $^ -o $@

%.o: %.c
	$(CC) $(CFLAGS) -c $<

%.o: %.S
	$(CC) $(CFLAGS) -c $<

$(VFLASH_IMAGE): $(TARGET_mkimage) $(PETITBOOT_IMAGE)
	./$(TARGET_mkimage) $(PETITBOOT_IMAGE) $@

install_petitboot_default: $(VFLASH_IMAGE)
	dd if=$< of=$(VFLASH_DEV_DEFAULT) bs=1 count=`wc -c $< | awk '{ print $$1 }'`

install_petitboot_linux: $(VFLASH_IMAGE)
	dd if=$< of=$(VFLASH_DEV_LINUX) bs=1 count=`wc -c $< | awk '{ print $$1 }'`

install_bootloader_default: $(TARGET_otheros_bootloader_loader)
	dd if=$< of=$(RAM_DEV) bs=1 count=`wc -c $< | awk '{ print $$1 }'` seek=$$(($(SLL_LOAD_LV2_OFFSET)))
	perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x1b"' | \
		dd of=$(RAM_DEV) bs=1 count=8 seek=$$(($(LOG2_PAGE_SIZE_OFFSET)))

install_bootloader_linux: $(TARGET_otheros_bootloader_loader)
	dd if=$< of=$(RAM_DEV) bs=1 count=`wc -c $< | awk '{ print $$1 }'` seek=$$(($(SLL_LOAD_LV2_OFFSET)))
	perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x1b"' | \
		dd of=$(RAM_DEV) bs=1 count=8 seek=$$(($(LOG2_PAGE_SIZE_OFFSET)))
	perl -e 'printf "\x00\x00\x00\x00\x00\x00\x00\x07"' | \
		dd of=$(RAM_DEV) bs=1 count=8 seek=$$(($(VFLASH_REGION_OFFSET)))

.PHONY: clean
clean:
	rm -f $(OBJ_otheros_bootloader_loader) $(TARGET_otheros_bootloader_loader)
	rm -f $(OBJ_mkimage) $(TARGET_mkimage)
	rm -f $(VFLASH_IMAGE)
