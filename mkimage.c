
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <arpa/inet.h>

static void usage(const char *progname);

static unsigned char header_magic[] =
{
	0x0F, 0xAC, 0xE0, 0xFF, 0xDE, 0xAD, 0xBA, 0xBE
};

/*
 * main
 */
int main(int argc, char **argv)
{
	FILE *in, *out;
	uint32_t size;
	unsigned char buf[4096];
	int nread, n;

	if (argc != 3)
		usage(argv[0]);

	in = fopen(argv[1], "r");
	if (in == NULL)
	{
		fprintf(stderr, "couldn't open file '%s'\n", argv[1]);
		exit(1);
	}

	out = fopen(argv[2], "w");
	if (out == NULL)
	{
		fprintf(stderr, "couldn't open file '%s'\n", argv[2]);
		exit(1);
	}


	n = fwrite(header_magic, 1, sizeof(header_magic), out);
	if (n != sizeof(header_magic))
	{
		fprintf(stderr, "fwrite failed\n");
		exit(1);
	}

	size = 0;

	n = fwrite(&size, 1, sizeof(size), out);
	if (n != sizeof(size))
	{
		fprintf(stderr, "fwrite failed\n");
		exit(1);
	}

	n = fseek(in, 0, SEEK_END);
	if (n < 0)
	{
		fprintf(stderr, "fseek failed\n");
		exit(1);
	}

	size = ftell(in);

	n = fseek(in, 0, SEEK_SET);
	if (n < 0)
	{
		fprintf(stderr, "fseek failed\n");
		exit(1);
	}

	size = htonl(size);

	n = fwrite(&size, 1, sizeof(size), out);
	if (n != sizeof(size))
	{
		fprintf(stderr, "fwrite failed\n");
		exit(1);
	}

	n = fseek(out, 0x400, SEEK_SET);
	if (n < 0)
	{
		fprintf(stderr, "fseek failed\n");
		exit(1);
	}

	while ((nread = fread(buf, 1, sizeof(buf), in)) > 0)
	{
		n = fwrite(buf, 1, nread, out);
		if (n != nread)
		{
			fprintf(stderr, "fwrite failed\n");
			exit(1);
		}
	}

	fclose(in);
	fclose(out);

	exit(0);
}

/*
 * usage
 */
static void usage(const char *progname)
{
	fprintf(stderr, "usage: %s <input file> <output file>\n", progname);
	exit(1);
}
